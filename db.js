var config = require('./config.js');
var env = process.env.NODE_ENV;
var knex = require('knex')(config.development.db);
module.exports = knex;
