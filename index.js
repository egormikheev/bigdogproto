var express = require('express')
var app = express()
var db = require('./db.js')
var bodyParser = require('body-parser')
var admin = require('firebase-admin')

admin.initializeApp({
  credential: admin.credential.cert('serviceKey.json'),
  databaseURL: 'https://bigdog-7b6c2.firebaseio.com',
  storageBucket: 'bigdog-7b6c2.appspot.com'
})

var birds = require('./routes/birds')
var gyms = require('./routes/gyms')
var crawler = require('./routes/crawler')

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

// app.use('/birds', birds);
app.use('/gyms', gyms)
app.use('/crawler', crawler)

db.migrate.latest().then(() => {
  app.listen(3000, () =>
    console.log(
      'Example app listening on port 3000! on environment:' +
        process.env.NODE_ENV
    )
  )
})
