let express = require('express')
let router = express.Router()
let db = require('../db.js')
let admin = require('firebase-admin')
let Utils = require('../utils')
let _ = require('lodash')
let snapshotToArray = new Utils().snapshotToArray

let gyms = admin.database().ref('/store')
let fs = admin.firestore()

router.get('/', function (req, res) {
  gyms.on(
    'value',
    function (snapshot) {
      res.json(snapshot.val())
    },
    function (errorObject) {
      console.log('The read failed: ' + errorObject.code)
    }
  )
})

router.get('/rewriteFromRawToFirestore', function (req, res) {
  let trains = admin.database().ref('/dictionaries/raw/trainings')
  trains.on('value', function (snapshot) {
    let trainings = snapshotToArray(snapshot)

    console.log('Got on: ' + trainings.length + ' trainings')
    console.log({ Sample: Object.keys(trainings[0].item) })
    console.log('HI')

    for (let train in trainings) {
      fs
        .collection('trains')
        .add({
          ...trainings[train].item,
          img: {
            ...trainings[train].item.img,
            musclesImgBase: null
          },
          userId: '0000000000000000000000000000'
        })
        .then(ref => {
          if (train == trainings.length - 1) {
            console.log(train + 1 + ' documents was added')
            res.send('OK')
          }
        })
    }
  })
})

async function getREf (fs, doc) {
  return new Promise((resolve, reject) => {
    fs
      .collection('trains')
      .where('uri', '==', doc.trainRef)
      .limit(1)
      .get()
      .then(value => {
        value.forEach(function (doc2) {
          doc.link = doc2.id
          // console.log(doc.ref)
          // // doc.data() is never undefined for query doc snapshots
          // console.log(doc.id, ' => ', doc.data())
          resolve(doc)
        })
      })
      .catch(e => {
        resolve(null)
      })
  })
}

router.get('/rawProgramsToFirestore', function (req, res) {
  let trains = admin.database().ref('/dictionaries/raw/programs')

  trains.on('value', function (snapshot) {
    let trainings = snapshotToArray(snapshot)

    console.log('Got on: ' + trainings.length + ' trainings')
    console.log({ Sample: Object.keys(trainings[0].item) })

    let promises = []
    for (let train in trainings) {
      // console.log(trainings[train])
      for (let program of trainings[train]['item'].programs) {
        if (program.trains && program.trains.length > 0) {
          for (let train1 of program.trains) {
            if (train1.trainRef) {
              promises.push(getREf(fs, train1))
            }
            // console.log(train1)
            // fs
            //   .collection('trains')
            //   .where('uri', '==', train1.trainRef)
            //   .limit(1)
            //   .get()
            //   .then(value => {
            //     value.forEach(function (doc) {
            //       train1.trainRef = doc
            //       // console.log(doc.ref)
            //       // // doc.data() is never undefined for query doc snapshots
            //       // console.log(doc.id, ' => ', doc.data())
            //     })
            //   })
          }
        }
      }
      // fs
      //   .collection('programs')
      //   .add({
      //     ...trainings[train].item,
      //     userId: '0000000000000000000000000000'
      //   })
      //   .then(ref => {
      //     if (train == trainings.length - 1) {
      //       console.log(train + 1 + ' documents was added')
      //       res.send('OK')
      //     }
      //   })
    }

    Promise.all(promises).then(values => {
      let grouped = _.groupBy(values, 'trainRef')
      for (let train in trainings) {
        for (let program in trainings[train]['item'].programs) {
          if (
            trainings[train]['item'].programs[program].trains &&
            trainings[train]['item'].programs[program].trains.length > 0
          ) {
            for (let train1 in trainings[train]['item'].programs[program]
              .trains) {
              trainings[train]['item'].programs[program].trains[train1].link =
                grouped[
                  trainings[train]['item'].programs[program].trains[
                    train1
                  ].trainRef
                ]
            }
          }
        }

        // console.log({ ...trainings[train]['item'] })

        // fs
        //   .collection('programs')
        //   .add({
        //     ...trainings[train]['item'],
        //     userId: '0000000000000000000000000000'
        //   })
        //   .then(ref => {
        //     if (train == trainings.length - 1) {
        //       console.log(train + 1 + ' documents was added')
        //       res.send('OK')
        //     }
        //   })
        //   .catch(e => {
        //     console.log(e)
        //   })
      }
    })
  })
})

router.get('/makedictionaries', function (req, res) {
  let dict = {
    equipment: [],
    level: [],
    muscleGroup: [],
    type: [],
    typeTrain: []
  }
  // получим значения в словари
  fs
    .collection('trains')
    .get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        dict.equipment.push(doc.data().equipment)
        dict.level.push(doc.data().level)
        dict.muscleGroup.push(doc.data().muscleGroup)
        dict.type.push(doc.data().type)
        dict.typeTrain.push(doc.data().typeTrain)
      })
      // запишем в коллекции уникальные значения
      dict.equipment = _.uniq(dict.equipment).filter(s => s)
      dict.level = _.uniq(dict.level).filter(s => s)
      dict.muscleGroup = _.uniq(dict.muscleGroup).filter(s => s)
      dict.type = _.uniq(dict.type).filter(s => s)
      dict.typeTrain = _.uniq(dict.typeTrain).filter(s => s)

      let batch = fs.batch()
      let train = fs.collection('dictionaries').doc('train')
      let eqRef = train.collection('equipment')
      let levRef = train.collection('level')
      let muscleGroup = train.collection('muscleGroup')
      let type = train.collection('type')
      let typeTrain = train.collection('typeTrain')

      dict.equipment.forEach(s => batch.set(eqRef.doc(), { name: s }))
      dict.level.forEach(s => batch.set(levRef.doc(), { name: s }))
      dict.muscleGroup.forEach(s => batch.set(muscleGroup.doc(), { name: s }))
      dict.type.forEach(s => batch.set(type.doc(), { name: s }))
      dict.typeTrain.forEach(s => batch.set(typeTrain.doc(), { name: s }))

      return batch.commit().then(function () {
        res.send('ok')
      })
    })
    .catch(e => {
      console.log(e)
    })
})

router.post('/', function (req, res) {
  gyms
    .child('gyms')
    .push(req.body)
    .then(snapshot => {
      res.json(req.body)
    })
    .catch(e => console.log(e))
})

module.exports = router
