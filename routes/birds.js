var express = require('express');
var router = express.Router();
var db  = require('../db.js');

router.get('/catalog', function(req, res) {
    db('typeBirds').then(rows => {
        res.json(rows);
    })
});

router.get('/', function(req, res) {
    db('birds').then(rows => {
        res.json(rows);
    }).catch(err => {
        res.sendStatus(501);
    })
});

router.post('/', function(req, res) {

    if(!req.body.birdType || !req.body.location) {
        res.sendStatus(400);
        
    } else {
        let date = new Date().toISOString().slice(0, 19).replace('T', ' ');
        
            const ins = {
                birdTypeId: req.body.birdType,
                location: req.body.location,
                created_at: date,
                updated_at: date
            }
        
            db('birds').insert(ins).then(rows => {
                ins.id = rows[0];
                res.json(ins);
            }).catch(err => {
                res.json(err);
            })
    }
});

module.exports = router;