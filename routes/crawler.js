var express = require('express')
var router = express.Router()
var db = require('../db.js')
var admin = require('firebase-admin')
// var base64Img = require('base64-img')
var request = require('request').defaults({ encoding: null })
let axios = require('axios')
let log = require('single-line-log').stdout
let Utils = require('../utils')
let snapshotToArray = new Utils().snapshotToArray
let FileAPI = require('file-api'), File = FileAPI.File
let _ = require('lodash')

var Crawler = require('crawler')

function timeout (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

function getPaginationArr (site, count) {
  let parseArr = []
  for (let i = 1; i <= count; i++) {
    parseArr.push(site + i)
  }
  return parseArr
}

async function getImage (url, mark) {
  let count = 0
  try {
    const { data } = await axios.get(url, {
      responseType: 'arraybuffer'
    })
    let name = url.split('/')[url.split('/').length - 1]
    var file = new File({
      name,
      type: 'image/jpeg', // optional
      buffer: data
    })
    return { data: file, mark, name }
  } catch (e) {
    count++
    if (count > 3) {
      return new Promise((resolve, reject) => {
        resolve(null)
      })
    } else {
      await timeout(1000)
      await getImage(url, mark)
    }
  }
}

async function uploadImage (bucket, item, path) {
  let prom = new Promise((resolve, reject) => {
    let fileUpload = bucket.file(path + '/' + item.name)
    const blobStream = fileUpload.createWriteStream({
      resumable: false,
      metadata: {
        contentType: item.data.type
      }
    })

    blobStream.on('error', error => {
      reject('Something is wrong! Unable to upload at the moment.')
    })

    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP.
      const url = `https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`

      resolve({ url, mark: item.mark, name: fileUpload.name,  })
    })
    blobStream.end(item.data.buffer)
  })
  return prom
}

function * uploadArrayImages (arrImages, bucket) {
  for (let item of arrImages) {
    if (item.mark) {
      let path = 'img/train/' + item.mark
      yield uploadImage(bucket, item, path)
    } else {
      return new Promise((resolve, reject) => {
        resolve(null)
      })
    }
  }
}

function * uploadImageObject (image, bucket) {
  if (image.mark) {
    let path = 'img/train/' + image.mark
    yield uploadImage(bucket, image, path)
  } else {
    return new Promise((resolve, reject) => {
      resolve(null)
    })
  }
}

function * getArrayRequests (arrUrl, mark) {
  for (let item of arrUrl) {
    yield getImage(item, mark)
  }
}

function * getObjectRequest (item, mark) {
  yield getImage(item, mark)
}

router.get('/getlist', function (req, res) {
  let hrefs = []
  let hrefsobj = {}
  let listRef = admin.database().ref('/dictionaries/raw/trainlist')

  var c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, resp, done) {
      if (error) {
        console.log(error)
      } else {
        var $ = resp.$
        // $ is Cheerio by default
        // a lean implementation of core jQuery designed specifically for the server
        let hyperlinks = $('.ui.fluid.card a.header ')
        for (let item in hyperlinks) {
          let href = hyperlinks[item]
          if (href.attribs && href.attribs.href) {
            hrefs.push(href.attribs.href)
          }
        }
      }
      done()
    }
  })

  let siteHead = 'https://dailyfit.ru'
  let site = siteHead + '/katalog-uprazhnenij/?pg='
  c.queue(getPaginationArr(site, 30))

  c.on('drain', function () {
    for (let item of hrefs) {
      listRef.push(item).then(data => {
        hrefsobj[data.key] = item
        if (hrefs[hrefs.length - 1] == item) {
          console.log(hrefsobj)
          res.json(hrefsobj)
        }
      })
    }
  })
})

router.get('/getprogrammslist', function (req, res) {
  let hrefs = []
  let hrefsobj = {}
  let listRef = admin.database().ref('/dictionaries/raw/programslist')

  var c = new Crawler({
    maxConnections: 10,
    // This will be called for each crawled page
    callback: function (error, resp, done) {
      if (error) {
        console.log(error)
      } else {
        var $ = resp.$
        // $ is Cheerio by default
        // a lean implementation of core jQuery designed specifically for the server
        let hyperlinks = $('a.card.cover')

        for (let item in hyperlinks) {
          let href = hyperlinks[item]
          if (href.attribs && href.attribs.href) {
            hrefs.push(href.attribs.href)
          }
        }
      }
      done()
    }
  })

  let siteHead = 'https://dailyfit.ru'
  let site = siteHead + '/programmy-trenirovok/?pg='
  c.queue(getPaginationArr(site, 19))

  c.on('drain', function () {
    for (let item of hrefs) {
      listRef.push(item).then(data => {
        hrefsobj[data.key] = item
        if (hrefs[hrefs.length - 1] == item) {
          console.log(hrefsobj)
          res.json(hrefsobj)
        }
      })
    }
  })
})

router.get('/parsetrainigs', function (req, res) {
  let siteHead = 'https://dailyfit.ru'
  trainings = []
  let trainingsHrefs = []
  let listRef = admin.database().ref('/dictionaries/raw/trainlist')
  let trainingsRef = admin.database().ref('/dictionaries/raw/trainings')
  let storageRef = admin.storage().bucket()

  listRef.on(
    'value',
    function (snapshot) {
      trainingsHrefs = snapshotToArray(snapshot)

      console.log('start parsing on' + trainingsHrefs.length + 'trainings')

      var c = new Crawler({
        maxConnections: 10,
        // This will be called for each crawled page
        callback: function (error, resp, done) {
          if (error) {
            console.log(error)
            done()
          } else {
            var $ = resp.$

            // $ is Cheerio by default
            // a lean implementation of core jQuery designed specifically for the server
            let body = $('#article-container')
            let header = body.find('h1.ui.header').text()
            let musclesImg = body.find('img').attr('src')
            let params = {}
            body.find('ul.ui.bulleted.list li').each(function (i, elem) {
              let line = $(this).text().split(':').map(s => s.trim())
              console.log(line)

              switch (line[0]) {
                case 'Группа мышц':
                  params.muscleGroup = line[1]
                  break
                case 'Тип упражнения':
                  params.type = line[1] ? line[1] : ''
                  break
                case 'Дополнительные мышцы':
                  params.addMuscles = line[1]
                    ? line[1].split(',').map(s => s.trim())
                    : ''
                  break
                case 'Вид упражнения':
                  params.typeTrain = line[1] ? line[1] : ''
                  break
                case 'Оборудование':
                  params.equipment = line[1] ? line[1] : ''
                  break
                case 'Уровень сложности':
                  params.level = line[1] ? line[1] : ''
                  break
              }
            })

            let techsteps = []
            body.find('.ui.segment ol li').each(function (i, elem) {
              techsteps[i] = $(this).text()
            })

            // get img refs
            let targetImgs = $(body)
              .find('.ui.secondary.pointing.menu')
              .nextAll()
            let maleImg = []
            let femaleImg = []
            $(targetImgs[0]).find('img').each(function (i, elem) {
              maleImg[i] = $(this).attr('src')
            })
            $(targetImgs[1]).find('img').each(function (i, elem) {
              femaleImg[i] = $(this).attr('src')
            })

            request.get(siteHead + musclesImg, function (error, response, body) {
              if (!error && response.statusCode == 200) {
                data =
                  'data:' +
                  response.headers['content-type'] +
                  ';base64,' +
                  new Buffer(body).toString('base64')

                let gen = getArrayRequests(maleImg, 'male')
                let gen2 = getArrayRequests(femaleImg, 'female')

                Promise.all([...gen, ...gen2]).then(values => {
                  values = values.filter(s => s)
                  console.log('All images was loaded', values.length, values[0])
                  let genIm = uploadArrayImages(values, storageRef)
                  Promise.all(genIm).then(val => {
                    console.log('uploaded images', val)

                    trainingsRef
                      .push({
                        header,
                        ...params,
                        steps: techsteps,
                        img: {
                          musclesImg: siteHead + musclesImg,
                          musclesImgBase: data || '',
                          maleImg,
                          femaleImg,
                          stored: val
                        },
                        uri: resp.options.uri
                      })
                      .then(data => {
                        done()
                      })
                  })
                })
              }
            })
          }
        }
      })

      // console.log(trainingsHrefs.map(s => s.item)[0])
      // lock =>
      c.queue(trainingsHrefs.map(s => s.item))

      c.on('drain', function () {
        console.log('All documents was uploaded')
        res.end('OK')
      })
    },
    function (errorObject) {
      console.log('The read failed: ' + errorObject.code)
    }
  )
})

router.get('/parseprogramm', function (req, res) {
  let siteHead = 'https://dailyfit.ru'
  trainings = []
  let trainingsHrefs = []
  let listRef = admin.database().ref('/dictionaries/raw/programslist')
  let programRef = admin.database().ref('/dictionaries/raw/programs')
  let storageRef = admin.storage().bucket()

  listRef.on(
    'value',
    function (snapshot) {
      trainingsHrefs = snapshotToArray(snapshot)

      console.log('start parsing on' + trainingsHrefs.length + 'trainings')

      var c = new Crawler({
        maxConnections: 10,
        // This will be called for each crawled page
        callback: function (error, resp, done) {
          if (error) {
            console.log(error)
            done()
          } else {
            var $ = resp.$

            // $ is Cheerio by default
            // a lean implementation of core jQuery designed specifically for the server
            let body = $('article.ui.segment')

            let image = body
              .find('header')
              .attr('style')
              .match(/(https[a-zA-Z0-9\/:\-\.]+)/g)[0]
            let header = body.find('header .header-container')
            let tname = header.find('h1').text()
            let tclass = header.find('.breadcrumb .section')
            // берем последнюю крошку
            let tclasslast
            tclass.each((index, item) => {
              tclasslast = $(item).text()
            })

            let programs = []

            body.find('.program-header').each(function (i, elem) {
              let name = $(elem).text()
              let trains = []
              $(elem).next().find('.item').each(function (i, elem) {
                let content = $(elem).find('.content')
                if (content) {
                  let trainRef = $(content).find('a').attr('href')
                  let description = $(content)
                    .find('.description')
                    .text()
                    .trim()
                  if (trainRef) {
                    trains.push({ trainRef, description })
                  }
                }
              })
              programs.push({ name, trains })
            })

            if (programs.length > 0) {
              let gen = getObjectRequest(image, 'program')
              gen.next().value.then(value => {
                console.log('Image was downloaded', value)
                let genIm = uploadImageObject(value, storageRef)
                genIm.next().value.then(val => {
                  console.log('uploaded images', val)
                  programRef
                    .push({
                      image: { href: image, stored: val },
                      header: tname,
                      class: tclasslast,
                      programs
                    })
                    .then(data => {
                      done()
                    })
                })
              })
            } else {
              done()
            }
          }
        }
      })

      // console.log(trainingsHrefs.map(s => s.item)[0])
      // lock =>
      c.queue(trainingsHrefs.map(s => s.item))

      c.on('drain', function () {
        console.log('All documents was uploaded')
        res.end('OK')
      })
    },
    function (errorObject) {
      console.log('The read failed: ' + errorObject.code)
    }
  )
})

module.exports = router
