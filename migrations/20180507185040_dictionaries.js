exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('gyms', function(table) {
      table.increments('uid').primary();
      table.string('title');
      table.string('description');
      table.timestamps();
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('gyms')]);
};
