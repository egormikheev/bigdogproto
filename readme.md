```bash
    knex migrate:make birds
```

* Обратим внимание на то, что у нас автоматически создался каталог `migrations`, в котором создался файл с таймштампом и именем миграции

```js
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('birds', function(table) {
      table.increments('uid').primary();
      table
        .integer('birdTypeId')
        .references('typeBirdsId')
        .inTable('typeBirds');
      table.string('location');
      table.timestamps();
    }),

    knex.schema.createTable('typeBirds', function(table) {
      table.increments('typeBirdsId').primary();
      table.string('title');
      table.string('description');
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('birds'),
    knex.schema.dropTable('typeBirds'),
  ]);
};
```

* Увидим заготовки под 2 экспортируемые функции
  * Первая функция создания, модификации таблиц
  * Вторая отката назад
* Заполним функции своими схемами
* выполним команду

```bash
     knex migrate:latest
```

* Просто запустим приложение, если ничего не произошло, а наш файл `mydb.sqlite` стал очень большим то нам судя по всему пока везет.

### Добавление данных в справочник

* Создадим еще одну миграцию в которой заполним таблицу видов птиц.
* Эта таблица является справочником, поэтому доступ через API к ней не предполагается (для простого пользователя)

```bash
    knex migrate:make insertBirdsType
```

```js
exports.up = function(knex, Promise) {
  return Promise.all([
    knex('typeBirds').insert([
      {
        title: 'Гагара Белоклювая',
        description: 'Лесная птица живет в средней полосе',
      },
      {
        title: 'Качурка северная',
        description: 'Лесная птица живет в Северо-Восточно части',
      },
      {
        title: 'Баклан Японский',
        description: 'Живет приемущественно вблизи берегов Японии',
      },
      {
        title: 'Нырок Белоглазый',
        description: 'Водоплавающая птица имеет широкий ареал распространения',
      },
    ]),
  ]);
};

exports.down = function(knex, Promise) {};
```

* Не забудем обновить базу данных

```bash
     knex migrate:latest
```
