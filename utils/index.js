function snapshotToArray (snapshot) {
  var returnArr = []
  snapshot.forEach(childSnapshot => {
    returnArr.push({ key: childSnapshot.key, item: childSnapshot.val() })
  })

  return returnArr
}

function Utils () {
  this.snapshotToArray = snapshotToArray
}

module.exports = Utils
